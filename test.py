#! /usr/bin/env python
################################################################################
#     File Name           :     test.py
#     Created By          :     totorikira
#     Creation Date       :     [2017-07-31 21:16]
#     Last Modified       :     [2017-07-31 22:35]
#     Description         :
################################################################################

import json


def main():
    row = 233
    print(bytes(str(row).encode('utf-8')))
    col = bytes(str(row).encode('utf-8'))
    col = int(col.decode('utf-8'))
    print(col)
    return


if __name__ == "__main__":
    main()
