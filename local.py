#! /usr/bin/env python
################################################################################
#     File Name           :     local.py
#     Created By          :     totorikira
#     Creation Date       :     [2017-07-31 22:21]
#     Last Modified       :     [2017-07-31 22:58]
#     Description         :     TotoriKira Local
################################################################################


import os
from scipy import ndimage
import re
import time
import json
import socket

def getCMD():
    print("\n请输入cURL（可从Chrome F12控制台得到）:")
    cURL = input()

    pattern = re.compile(
        r'x_min=[0-9]+&y_min=[0-9]+&x_max=[0-9]+&y_max=[0-9]+&color=[0-9]+')
    request = pattern.sub(
        r"x_min={0}&y_min={1}&x_max={0}&y_max={1}&color={2}", cURL)
    request += '  --silent'

    return request


def main():
    # Get color map info
    tmp = '''
{
"(5, 113, 151)":"B",
"(113, 190, 214)":5,
"(240, 253, 243)":1,
"(255, 246, 209)":2,
"(151, 253, 220)":"C",
"(66, 213, 213)":6,
"(248, 203, 140)":"F",
"(0, 70, 112)":"A",
"(19, 123, 159)":"B",
"(184, 63, 39)":"E",
"(59, 229, 219)":6,
"(16, 87, 126)":"A",
"(96, 178, 204)":5,
"(46, 143, 175)":"B",
"(125, 149, 145)":4,
"(90, 206, 216)":6
}
    '''
    tmp = (json.loads(tmp))

    # Get gif map info
    im_array = ndimage.imread("ref.gif")
    # 行  列

    # Start output process
    request = getCMD()


    while True:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect(( "127.0.0.1", 2333 ));
        i = s.recv(1024) # 行
        s.send(b"")
        print(i)
        if (i==b"finish"):
            print("Finished Drawing")
            print("Finished Drawing")
            print("Finished Drawing")
            return

        s.send(b"break")
        j = s.recv(1024) # 列
        print(j)

        i = int(i.decode("utf-8"))
        j = int(j.decode("utf-8"))

        print("绘图位置：{0}  {1}".format(j, i))
        while True:
            ret = os.popen(request.format(j, i, tmp[str(tuple(im_array[i][j]))])).readlines()[0]
            print("颜色信息(RGB)：", im_array[i][j])
            print("对应画笔信息：", tmp[str(tuple(im_array[i][j]))])
            print("网络请求信息：", ret)
            ret=json.loads(ret)
            if ret['msg'] == 'success':
                break
            elif ret['msg'] == '请先登录哦':
                request=getCMD()
            time.sleep(int(ret['data']['time']))
        s.close()

    return


if __name__ == "__main__":
    main()
