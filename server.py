#! /usr/bin/env python
################################################################################
#     File Name           :     server.py
#     Created By          :     totorikira
#     Creation Date       :     [2017-07-31 22:02]
#     Last Modified       :     [2017-07-31 22:58]
#     Description         :     TotoriKira Brush Server
################################################################################

import socket
import threading
import time

lock = threading.Lock()
row = 1
col = 1
MaxRow = 720
MaxCol = 1280

def newConnection(s, addr):
    global row, col, lock
    print("New Connection From {0}".format(addr))
    try:
        lock.acquire()
        if row == MaxRow:
            s.send(b'finish')
            return
        s.send(str(row).encode("utf-8"))
        s.recv(1024)
        s.send(str(col).encode("utf-8"))
        col += 1
        if col == MaxCol:
            row += 1
            col = 1
        lock.release()

    except Exception as e:
        lock.release()

    return


def main():

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(("0.0.0.0", 2333))

    s.listen(5)

    while True:
        try:
            sock, addr = s.accept()

            t = threading.Thread(target=newConnection, args=(sock, addr))
            t.start()
        except Exception as e:
            pass

    return


if __name__ == "__main__":
    main()
