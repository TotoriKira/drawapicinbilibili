from cx_Freeze import setup, Executable
import scipy
import PIL
import os

includefiles_list=[]
scipy_path = os.path.dirname(scipy.__file__)
includefiles_list.append(scipy_path)

PIL_path = os.path.dirname(PIL.__file__)
includefiles_list.append(PIL_path)

# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(packages = ["numpy", "PIL"], excludes = [], include_files=includefiles_list)

base = 'Console'

executables = [
    Executable('draw.py', base=base)
]

setup(name='draw',
      version = '1.0',
      description = '用于Bilibili的夏日绘版活动',
      options = dict(build_exe = buildOptions),
      executables = executables)
